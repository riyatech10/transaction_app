
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/slider.dart';


class ProfileTap extends StatefulWidget {
  _ProfileTapState createState() => _ProfileTapState();
}

class _ProfileTapState extends State<ProfileTap> {
  double _value=0.0;
 @override
void _onChanged(double value){
   setState(() {
     _value=value;
   });

 }
 Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new Column(
        children: <Widget>[
         new Row(
           children: <Widget>[
             new Container(
               alignment: Alignment.bottomCenter,
               height: 50.0,
               width: 200.0,
               child: Text('Select Amount',style: TextStyle(
                 fontSize: 20.0,
                 fontWeight: FontWeight.w400,
                 letterSpacing: 1.0,
               ),),
             ),
           ],
         ),
         new Column(
           children: <Widget>[
             new Container(
               height: 160.0,
               margin: EdgeInsets.only(top: 35.0),
               child: new Container(
                 height: 160.0,
                 width: 160.0,
                 decoration: new BoxDecoration(
                     image: const DecorationImage(
                       fit: BoxFit.fill,
                       image: const AssetImage('assets/profilephoto.jpg'),
                     ),
                     shape: BoxShape.circle,
                     border: new Border.all(
                         color: Colors.deepPurple,
                         width: 3.0
                     )
                 ),
               ),
             ),
             new Container(
               margin: EdgeInsets.only(top: 10.0),
               child: new Text('Kathryn',
               style: TextStyle(
                 fontSize: 20.0,
                 fontWeight: FontWeight.w600,
                 letterSpacing: 0.5
               ),),
             ),
            new Column(
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(left:20.0,right: 20.0,
                      top: 30.0),
                  height: 50.0,
                  width: 300.0,
                  decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(5.0),
                      border: new Border.all(
                          color: Colors.grey,
                          width: 2.0
                      )
                  ),
                  child: Text(' ${_value.round()}',style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.w600,
                  ),),
                ),
               new Slider(
                     min: 0.0,
                     max: 1000.0,
                     activeColor: Colors.deepPurple,
                     inactiveColor: Colors.grey,
                     value: _value, onChanged:(double value){ _onChanged(value);
                 }
                 ),


              ],
            )
           ],
         )
        ],

      ),
    );
  }

  
}