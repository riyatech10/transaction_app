import 'package:flutter/material.dart';
import 'package:swipedetector/swipedetector.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'SendToPage.dart';
import 'ReceiveToPage.dart';
class Swipe_Page extends StatefulWidget {
  @override
  _Swipe_PageState createState() => new _Swipe_PageState();
}

class _Swipe_PageState extends State<Swipe_Page> {
  @override
//  void _onSwipe(){Navigator.push(
//    context,
//    MaterialPageRoute(builder: (context) => ParentAnim()),
//  );}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: new Stack(
          children: <Widget>[
            new Positioned(child:  SwipeDetector(
              child:  new Transform(
               child: new Material(
                 child:  Container(
      height: 50.0,
        width: 180.0,

        child: Container(
          height: 20.0,
          width: 20.0,
          alignment: Alignment.center,
          child: Text('swipe to send',style: TextStyle(
            fontWeight: FontWeight.w400,
            letterSpacing: 0.5,
            color: Colors.white,
          ),),
        ),

      ),
                 color: Colors.green,
                 borderRadius: BorderRadius.circular(25.0),
                 elevation: 20.0,
               ),
                alignment: FractionalOffset.center,
                transform: new Matrix4.identity()
                  ..rotateZ(-41 * 3.1415927 / 180),
              ),

              onSwipeRight: () {
                setState(() {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SendToPage()),
                  );
                });
              },

              swipeConfiguration: SwipeConfiguration(
                  verticalSwipeMinVelocity: 0.0,
                  verticalSwipeMinDisplacement: 0.0,
                  verticalSwipeMaxWidthThreshold:130.0,
                  horizontalSwipeMaxHeightThreshold: 250.0,
                  horizontalSwipeMinDisplacement:0.0,
                  horizontalSwipeMinVelocity: 0.0),
            ),
              top: 280.0,
              left: 140.0,
            ),
       new Positioned(child:  SwipeDetector(
         child:  new Transform(
          child: new Material(
            child: Container(
    height: 50.0,
    width: 180.0,
    decoration: new BoxDecoration(
    borderRadius: BorderRadius.circular(25.0),
    color: Colors.red,
    ),
    child: Container(
    height: 20.0,
    width: 20.0,
    alignment: Alignment.center,
    child: Text('swipe to Receive',style: TextStyle(
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5,
    color: Colors.white,
    ),),
    )
    ),
            color: Colors.red,
            borderRadius: BorderRadius.circular(25.0),
            elevation: 20.0,
          ),
           alignment: FractionalOffset.center,
           transform: new Matrix4.identity()
             ..rotateZ(41 * 3.1415927 / 180),
         ),

         onSwipeRight: () {
           setState(() {
             Navigator.push(
               context,
               MaterialPageRoute(builder: (context) => ReceiveToPage()),
             );
           });
         },


         swipeConfiguration: SwipeConfiguration(
             verticalSwipeMinVelocity: 0.0,
             verticalSwipeMinDisplacement: 0.0,
             verticalSwipeMaxWidthThreshold:130.0,
             horizontalSwipeMaxHeightThreshold: 250.0,
             horizontalSwipeMinDisplacement:0.0,
             horizontalSwipeMinVelocity: 0.0),
       ),
       bottom: 272.0,
         left: 132.0,
       ),




//            new Positioned(child:  new GestureDetector(
//              child:  new Container(
//                height: 60.0,
//                width: 60.0,
//                padding: new EdgeInsets.all(5.0),
//                child: new CircleAvatar(
//                  backgroundColor: Colors.black,
//                ),
//              ),
//            ),
//            left: 130.0,
//              bottom: 320.0,
//            ),


          ],
        ),

    );
  }
}