import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'ParentAnim.dart';
class DelayAnim extends StatefulWidget {
  _DelayAnimState createState() => _DelayAnimState();
}

class _DelayAnimState extends State<DelayAnim> with SingleTickerProviderStateMixin {

  AnimationController _animationController;
  Animation animation, delayedAnimation, muchDelayedAnimation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        duration: Duration(seconds: 5),
        vsync: this);
    animation = Tween(begin: -0.1, end: 0.1).animate(CurvedAnimation
      (parent: _animationController,
        curve: Curves.fastOutSlowIn)
    );
    delayedAnimation = Tween(begin: -0.1, end: 0.1).animate(CurvedAnimation
      (parent: _animationController,
        curve: Interval(0.5, 1.0, curve: Curves.fastOutSlowIn))
    );
    muchDelayedAnimation = Tween(begin: -0.1, end: 0.1).animate(CurvedAnimation
      (parent: _animationController,
        curve: Interval(0.8, 1.0, curve: Curves.fastOutSlowIn))
    );
    _animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery
        .of(context)
        .size
        .width;
    // TODO: implement build
    return AnimatedBuilder(animation: _animationController,
        builder: (BuildContext context, Widget child) {
          return Scaffold(
              body: new Align(
                alignment: Alignment.topLeft,
                child: new Container(
                  child: new ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Transform(
                        transform: Matrix4.translationValues(
                            animation.value * width, 0.0, 0.0),
                        child: new Container(
                          padding: EdgeInsets.all(25.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/profilephoto.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/profilephoto.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/profilephoto.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/profilephoto.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),


                            ],
                          ),
                        ),

                      ),
                      Transform(
                        transform: Matrix4.translationValues(
                            delayedAnimation.value * width, 0.0, 0.0),
                        child: new Container(
                          padding: EdgeInsets.all(25.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/timeline.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),

                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/timeline.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/timeline.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),
                              new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/timeline.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),

                            ],
                          ),
                        ),
                      ),

                      new Container(
                        padding: EdgeInsets.all(25.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new GestureDetector(
                              child:  new Container(
                                child: new CircleAvatar(
                                  backgroundImage: new AssetImage(
                                      'assets/greenleaf.jpg'),
                                ),
                                margin: EdgeInsets.all(10.0),
                              ),
                              onTap: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => ParentAnim()),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
          );
        });
  }
}