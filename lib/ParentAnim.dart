import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'FullProfile.dart';
class ParentAnim extends StatefulWidget {
  _ParentAnimState createState() => _ParentAnimState();
}

class _ParentAnimState extends State<ParentAnim> with SingleTickerProviderStateMixin {

  AnimationController animationController;
  Animation animation, childAnimation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(seconds: 3),
        vsync: this);
    animation = Tween(begin: 0.1, end: -0.1).animate(CurvedAnimation
      (parent: animationController,
        curve: Curves.fastOutSlowIn));
    childAnimation = Tween(begin: 50.0,end: 330.0).animate(
        CurvedAnimation(
        parent:animationController ,
        curve: Curves.fastOutSlowIn)
    );


  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
    builder: (BuildContext context, Widget child)
    {
      return Scaffold(
        appBar: AppBar(
          title: Text('Send To'),
        ),
        body:
        new Align(
          alignment: Alignment.bottomLeft,
          child: new Container(
            width: 450.0,

            child: Transform(
              transform: Matrix4.translationValues(
                  0.0,animation.value * width, 0.0),
              child: new AnimatedBuilder(
                  animation: childAnimation,
                  builder: (BuildContext context, Widget child)
                  {
                    return Container(
                      height: childAnimation.value*2,
                      width: childAnimation.value*2,

                      child: new ListView(

                        children: <Widget>[
                          Container(
                            //padding: EdgeInsets.all(25.0),
                            child: new Stack(
                              children: <Widget>[
                                new Positioned(
                                  child:GestureDetector(
                                    child: new Container(
                                      child: new CircleAvatar(
                                        backgroundImage: new AssetImage(
                                            'assets/profilephoto.jpg'),
                                      ),
                                      margin: EdgeInsets.all(25.0),
                                      height: 90.0,
                                      width: 90.0,
                                    ),
                                    onTap: (){
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => FullProfile()),
                                      );
                                    },
                                  ),

                                  bottom: 25.0,
                                ),
                                new Positioned(child: Container(
                                 child:Text('Kumar',style: TextStyle(
                                   fontSize: 15.0,
                                   fontWeight: FontWeight.w600,
                                   letterSpacing: 0.5
                                 ),),


                                  alignment: Alignment.center,
                                ),
                                  bottom: 25.0,
                                  left: 35.0,
                                ),
                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),

                                  left: 130.0,


                                ),
                                new Positioned(child: Container(
                                  child:Text('Ashwanth',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),
//

                                  alignment: Alignment.center,
                                ),
                                  bottom: 60.0,
                                  left: 160.0,
                                ),

                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),
                                 left: 260.0,
                                  bottom: 25.0,
                                ),
                                new Positioned(child: Container(
                                  child:Text('Kumar',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),


                                  alignment: Alignment.center,
                                ),
                                  bottom: 25.0,
                                  left: 300.0,
                                ),
                              ],
                            ),
                            height: 200.0,
                            width: 300.0,
                          ),

                          Container(
                            //padding: EdgeInsets.all(25.0),
                            child: new Stack(
                              children: <Widget>[
                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),

                                  bottom: 25.0,
                                ),
                                new Positioned(child: Container(
                                  child:Text('Kumar',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),

                                  alignment: Alignment.center,
                                ),
                                  bottom: 25.0,
                                  left: 35.0,
                                ),
                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),

                                  left: 130.0,


                                ),
                                new Positioned(child: Container(
                                  child:Text('Ashwanth',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),
//

                                  alignment: Alignment.center,
                                ),
                                  bottom: 60.0,
                                  left: 160.0,
                                ),

                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),
                                  left: 260.0,
                                  bottom: 25.0,
                                ),
                                new Positioned(child: Container(
                                  child:Text('Kumar',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),


                                  alignment: Alignment.center,
                                ),
                                  bottom: 25.0,
                                  left: 300.0,
                                ),
                              ],
                            ),
                            height: 200.0,
                            width: 300.0,
                          ),
                          Container(
                            //padding: EdgeInsets.all(25.0),
                            child: new Stack(
                              children: <Widget>[
                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),

                                  bottom: 25.0,
                                ),
                                new Positioned(child: Container(
                                  child:Text('Kumar',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),


                                  alignment: Alignment.center,
                                ),
                                  bottom: 25.0,
                                  left: 35.0,
                                ),
                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),

                                  left: 130.0,


                                ),
                                new Positioned(child: Container(
                                  child:Text('Ashwanth',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5
                                  ),),
//
//
                                  alignment: Alignment.center,
                                ),
                                  bottom: 60.0,
                                  left: 160.0,
                                ),

                                new Positioned(
                                  child:new Container(
                                    child: new CircleAvatar(
                                      backgroundImage: new AssetImage(
                                          'assets/profilephoto.jpg'),
                                    ),
                                    margin: EdgeInsets.all(25.0),
                                    height: 90.0,
                                    width: 90.0,
                                  ),
                                  left: 260.0,
                                  bottom: 25.0,
                                ),
                                new Positioned(child: Container(
                                  child:Text('Kumar',style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5

                                  ),),

                                  alignment: Alignment.center,
                                ),
                                  bottom: 25.0,
                                  left: 300.0,
                                ),
                              ],
                            ),
                            height: 200.0,
                            width: 300.0,
                          ),
                        ],
                      ),

                    );
                  }
              ),
            ),

          ),
        )
      );
    }
    );
  }
}