import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'FullProfile.dart';
class SendToPage extends StatefulWidget {
  _ParentAnimState createState() => _ParentAnimState();
}

class _ParentAnimState extends State<SendToPage> with SingleTickerProviderStateMixin {

  AnimationController animationController;
  Animation animation, childAnimation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(seconds: 1),
        vsync: this);
    animation = Tween(begin: 0.015, end: -0.1).animate(CurvedAnimation
      (parent: animationController,
        curve: Curves.fastOutSlowIn));
    childAnimation = Tween(begin: 300.0,end: 300.0).animate(
        CurvedAnimation(
            parent:animationController ,
            curve: Curves.fastOutSlowIn)
    );


  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child)
        {
          return Scaffold(
            appBar: AppBar(
              title: Text('Send To',style: TextStyle(
                  color: Colors.black,
                  fontSize: 25.0,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.5
              ),),

              backgroundColor: Colors.white,
              elevation: 0.0,
              actions: <Widget>[
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(right: 15.0),
                  child:  Text('cancel',style: TextStyle(
                      color: Colors.grey,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5

                  ),),
                )
              ],
            ),
            body:
            Column(
              children: <Widget>[
                Container(
                  height: 70.0,
                  child: new Row(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(left:20.0,bottom: 10.0),
                          height: 35.0,
                          width: 330.0,
                          decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Color(0xFFE0E0E0),
                              borderRadius: BorderRadius.circular(15.0),
                              border: new Border.all(
                                  color: Colors.grey,
                                  width: 1.0
                              )
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Container(
                                child: Text('Search',style: TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xFF757575),
                                  fontWeight: FontWeight.w600,
                                ),),
                                margin: EdgeInsets.only(left: 10.0),
                              ),
                              IconButton(icon: Icon(Icons.search,size: 25.0,color: Color(0xFF757575),), onPressed: (){}),
                            ],
                          )
                      ),
                      new IconButton(icon: Icon(Icons.add),
                        iconSize: 28.0,
                        highlightColor: Colors.green,color:Colors.green,
                        onPressed: (){},)
                    ],
                  ),
                ),
                new Align(
                  alignment: Alignment.bottomCenter,
                  child: new Container(
//                     height: childAnimation.value*2,
//                     width: childAnimation.value*2,
                    child: Transform(
                      transform: Matrix4.translationValues(
                          0.0,animation.value * width, 0.0),
                      child: new AnimatedBuilder(
                          animation: childAnimation,
                          builder: (BuildContext context, Widget child)
                          {
                            return Container(
                              height: childAnimation.value*2,
                              width: childAnimation.value*2,
                              child: Center(
                                child:  new ListView(
                                  children: <Widget>[
                                    Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          new Positioned(
                                            child:GestureDetector(
                                              child: new Container(
                                                child: new CircleAvatar(
                                                  backgroundImage: new AssetImage(
                                                      'assets/profile3.jpg'),
                                                ),
                                                margin: EdgeInsets.all(25.0),
                                                height: 90.0,
                                                width: 90.0,

                                              ),
                                              onTap: (){
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(builder: (context) => FullProfile()),
                                                );
                                              },
                                            ),

                                            bottom: 25.0,
                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Kumar',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),


                                            alignment: Alignment.center,
                                          ),
                                            bottom: 25.0,
                                            left: 35.0,
                                          ),
                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),

                                            left: 130.0,


                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Ashwanth',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),
//

                                            alignment: Alignment.center,
                                          ),
                                            bottom: 60.0,
                                            left: 160.0,
                                          ),

                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),
                                            left: 260.0,
                                            bottom: 25.0,
                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Kumar',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),


                                            alignment: Alignment.center,
                                          ),
                                            bottom: 25.0,
                                            left: 300.0,
                                          ),
                                        ],
                                      ),
                                      height: 200.0,
                                      width: 300.0,

                                    ),

                                    Container(
                                      //padding: EdgeInsets.all(25.0),
                                      child: new Stack(
                                        children: <Widget>[
                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),

                                            bottom: 25.0,
                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Kumar',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),

                                            alignment: Alignment.center,
                                          ),
                                            bottom: 25.0,
                                            left: 35.0,
                                          ),
                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),

                                            left: 130.0,


                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Ashwanth',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),
//

                                            alignment: Alignment.center,
                                          ),
                                            bottom: 60.0,
                                            left: 160.0,
                                          ),

                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),
                                            left: 260.0,
                                            bottom: 25.0,
                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Kumar',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),


                                            alignment: Alignment.center,
                                          ),
                                            bottom: 25.0,
                                            left: 300.0,
                                          ),
                                        ],
                                      ),
                                      height: 200.0,
                                      width: 300.0,
                                    ),
                                    Container(
                                      //padding: EdgeInsets.all(25.0),
                                      child: new Stack(
                                        children: <Widget>[
                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),

                                            bottom: 25.0,
                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Kumar',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),


                                            alignment: Alignment.center,
                                          ),
                                            bottom: 25.0,
                                            left: 35.0,
                                          ),
                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),

                                            left: 130.0,


                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Ashwanth',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5
                                            ),),
//
//
                                            alignment: Alignment.center,
                                          ),
                                            bottom: 60.0,
                                            left: 160.0,
                                          ),

                                          new Positioned(
                                            child:new Container(
                                              child: new CircleAvatar(
                                                backgroundImage: new AssetImage(
                                                    'assets/profile3.jpg'),
                                              ),
                                              margin: EdgeInsets.all(25.0),
                                              height: 90.0,
                                              width: 90.0,
                                            ),
                                            left: 260.0,
                                            bottom: 25.0,
                                          ),
                                          new Positioned(child: Container(
                                            child:Text('Kumar',style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.5

                                            ),),

                                            alignment: Alignment.center,
                                          ),
                                            bottom: 25.0,
                                            left: 300.0,
                                          ),
                                        ],
                                      ),
                                      height: 200.0,
                                      width: 300.0,
                                    ),
                                  ],
                                ),
                              ),

                            );
                          }
                      ),
                    ),

                  ),
                )
              ],
            ),


          );
        }
    );
  }
}