import 'package:flutter/material.dart';
class MyHome extends StatefulWidget {
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> with TickerProviderStateMixin {

  AnimationController animationController,delayedAnimation,muchDelayedAnimation;
  Animation _animation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(seconds: 8),
        vsync: this);
    _animation = Tween(begin: 0.15, end: -0.1).animate(CurvedAnimation
      (parent: animationController,
        curve: Curves.fastOutSlowIn)
    );

  }

  @override
  Widget build(BuildContext context) {
    final width =MediaQuery.of(context).size.width;
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context,Widget child){
          return new Scaffold(
              body:   new Center(
                child: new GestureDetector(
                  child: new Container(
                    alignment: Alignment.bottomCenter,
                    width: 300.0,
                    height: 200.0,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: const DecorationImage(
                        fit: BoxFit.fill,
                        image: const AssetImage('assets/timeline.jpg'),
                      ),
                    ),
                    transform: Matrix4.translationValues(0.0, _animation.value*width, 0.0),
                  ),
                  onTap: (){
                    animationController.forward();
                  },
                  onDoubleTap: (){
                    animationController.reverse();
                  },
                ),

              )
          );
        });
  }
}