import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'Purchase.dart';
class FullProfile extends StatefulWidget {
  _FullProfileState createState() => _FullProfileState();
}

class _FullProfileState extends State<FullProfile> with SingleTickerProviderStateMixin {


  static const Duration enableAnimationDuration = Duration(milliseconds: 75);
  double _value=0.0;
  @override

  void _onChanged(double value){
    setState(() {
      _value=value;
    });

  }
  AnimationController animationController;
  Animation animation, childAnimation;
  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(seconds: 2),
        vsync: this);
    animation = Tween(begin: -0.1, end: 0.0).animate(CurvedAnimation
      (parent: animationController,
        curve: Curves.fastOutSlowIn));
    childAnimation = Tween(begin: 20.0, end: 500.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Curves.fastOutSlowIn)
    );
  }


  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = new GlobalKey<ScaffoldState>();
    final width = MediaQuery.of(context).size.width;
    animationController.forward();
    void _persistentBottomSheet(){
      showModalBottomSheet(context:context,builder:(builder){
        return new Container(
          decoration: new BoxDecoration(
            borderRadius: new BorderRadius.only(
                topLeft:  Radius.circular(40.0),
                topRight:  Radius.circular(40.0)),
            color: Colors.white,
          ),
          height: 350.0,
          child: new Column(
            children: <Widget>[
              new Row(
children: <Widget>[
  new Container(
    decoration: new BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(10.0),
        border: new Border.all(
            color: Colors.grey,
            width: 2.0
        )
    ),
    margin: EdgeInsets.only(top: 20.0,left: 10.0),
    height: 30.0,
    width: 380.0,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
       new Container(
         child:  Text('Search',style: TextStyle(
           fontSize: 20.0,
           fontWeight: FontWeight.w400,
           letterSpacing: 0.5
         ),),
         margin: EdgeInsets.only(left: 10.0),
       ),
        new Container(
          margin: EdgeInsets.only(right: 10.0),
           child: Icon(Icons.search,size: 30.0,)
        )
      ],
    ),
  )
],
              ),
              new Padding(padding: EdgeInsets.all(40.0),
                child:  new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                      GestureDetector(
                       child:  Container(
                         height: 20.0,
                         width: 35.0,
                         decoration: new BoxDecoration(
                           image: const DecorationImage(
                             fit: BoxFit.fill,
                             image: const AssetImage('assets/flag6.png'),
                           ),
                         ),
                         transform: Matrix4.translationValues(0.0, animation.value*width, 0.0),
                       ),
                       onTap: (){
                         animationController.forward();
                       },
                     ),




                    Container(
                      height: 20.0,
                      width: 35.0,
                      decoration: new BoxDecoration(
                        image: const DecorationImage(
                          fit: BoxFit.fill,
                          image: const AssetImage('assets/flag2.png'),
                        ),
                      ),
                    ),
                    Container(
                      height: 20.0,
                      width: 35.0,
                      decoration: new BoxDecoration(
                        image: const DecorationImage(
                          fit: BoxFit.fill,
                          image: const AssetImage('assets/flag3.png'),
                        ),
                      ),
                    ),
                  ],
                ),

              ),

            ]
          ),
        );
      });
    }
    return
      new AnimatedBuilder(
          animation: animationController,
          builder: (BuildContext context, Widget child)
          {
            return new Scaffold(

                key: _scaffoldKey,
              appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 0.0,
                centerTitle: true,
                title: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: 200.0,
                  child: new Text('Select Amount',style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey,
                    fontWeight: FontWeight.w600
                  ),),
                ),
              ),
                body: new Align(
                  alignment: Alignment.bottomCenter,
                  child: new Container(

                    child: Transform(
                      transform: Matrix4.translationValues(
                          0.0,animation.value * width, 0.0),
    child: new AnimatedBuilder(
    animation: childAnimation,
    builder: (BuildContext context, Widget child)
    {
    return Container(
      height: childAnimation.value*2,
      width: childAnimation.value*2,
      child: new Center(
        child: new Container(
          height: childAnimation.value*2,
          width: childAnimation.value*2,
          child:new Center(
           child: Stack(
             children: <Widget>[
               new Positioned(
                 top: 50.0,
                 left: 120.0,
                 child: Container(
                 child: new CircleAvatar(
                   backgroundImage: new AssetImage(
                       'assets/profilephoto.jpg'),
                 ),
                   decoration: new BoxDecoration(
                       shape: BoxShape.circle,
                       border: new Border.all(
                           color: Colors.grey,
                           width: 3.0
                       )
                   ),
                 height: 180.0,
                 width: 180.0,
               ),),
               new Positioned(child:  new Container(
                 child: new Text('Kathryn',
                   style: TextStyle(
                       fontSize: 20.0,
                       fontWeight: FontWeight.w600,
                       letterSpacing: 0.5
                   ),),
               ),
               height: 20.0,
                 top: 260.0,
                 left: 170.0,
               ),
               new Positioned(child: new Container(
                 margin: EdgeInsets.only(left:20.0,right: 20.0,
                     top: 30.0),
                 height: 50.0,
                 width: 330.0,
                 decoration: new BoxDecoration(
                     shape: BoxShape.rectangle,
                     borderRadius: BorderRadius.circular(15.0),
                     border: new Border.all(
                         color: Colors.grey,
                         width: 2.0
                     )
                 ),
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     new Container(
                      child: Text('${_value.round()}',style: TextStyle(
                        fontSize: 30.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w600,
                      ),),
                       margin: EdgeInsets.only(left: 10.0),
                     ),
                     IconButton(icon: Icon(Icons.keyboard_arrow_down,size: 30.0,color: Colors.grey,), onPressed: _persistentBottomSheet)
                   ],
                 )
               ),
               top: 290.0,
                 left: 15.0,
               ),
             new Container(
               child:  new Positioned(child:  new Slider(
                   min: 0.0,
                   max: 1000.0,
                   activeColor: Colors.deepPurple,
                   inactiveColor: Colors.grey,
                   value: _value, onChanged:(double value){ _onChanged(value);
               }
               ),
                 bottom: 180.0,
                 left: 20.0,
                 width: 350.0,
               ),
             ),

             ],
           )
          )
        ),
      ),
    );
    }
    ),
    ),
                  ),
                ),

              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Purchase()),
                  );
                },
                child: Icon(Icons.arrow_forward,color: Colors.white,),
               backgroundColor: Colors.deepPurple,
              ),

            );
          }
      );
  }
}