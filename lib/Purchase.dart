import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class Purchase extends StatefulWidget {
  _Purchase createState() => _Purchase();
}

class _Purchase extends State<Purchase> with SingleTickerProviderStateMixin {
  static const Duration enableAnimationDuration = Duration(milliseconds: 75);
  double _value = 0.0;

  @override
  void _onChanged(double value) {
    setState(() {
      _value = value;
    });
  }

  AnimationController animationController;
  Animation animation, childAnimation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(seconds: 2),
        vsync: this);
    animation = Tween(begin: -0.1, end: 0.0).animate(CurvedAnimation
      (parent: animationController,
        curve: Curves.fastOutSlowIn));
    childAnimation = Tween(begin: 20.0, end: 20.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Curves.fastOutSlowIn)
    );
  }
  @override
  Widget build(BuildContext context) {
    bool selected = false;
    final width = MediaQuery.of(context).size.width;
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child)
        {
          return new Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 0.0,
                centerTitle: true,
                title: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: 200.0,
                  child: new Text('Purpose', style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.grey,
                      fontWeight: FontWeight.w600
                  ),),
                ),
              ),
              body: new Container(
                padding: EdgeInsets.all(20.0),
                child: new GridView.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 20.0,
                  mainAxisSpacing: 20.0,
                  children: new List<Widget>.generate(4, (index) {
                    return new GridTile(
                      child: new Transform(transform: Matrix4.translationValues(
                          0.0, animation.value * width, 0.0),
                          child: new AnimatedBuilder(animation: childAnimation,
                            builder: (BuildContext context, Widget child) {
                              return new Container(
                                  child: new Card(
                                      shape: selected
                                          ? new RoundedRectangleBorder(
                                          side: new BorderSide(color: Colors.blue, width: 2.0),
                                          borderRadius: BorderRadius.circular(4.0))
                                          : new RoundedRectangleBorder(
                                          side: new BorderSide(color: Colors.white, width: 2.0),
                                          borderRadius: BorderRadius.circular(4.0)),
                                      color: Colors.white,
                                      child: new Center(
                                        child: new Container(
                                          margin: EdgeInsets.only(bottom: 20.0),
                                          decoration: new BoxDecoration(

                                            borderRadius: BorderRadius.circular(15.0),
                                            image: const DecorationImage(

                                              image: const AssetImage(
                                                  'assets/timeline.jpg'),
                                            ),
                                          ),
                                        ),
                                      )
                                  ),


                              );
                            },


                          )
                      ),);
                  }),
                ),
              )
          );
        });
  }
}