import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'Swipe_Page.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => new _SplashState();
}

class _SplashState extends State <Splash> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Money Doesn't Grow On Trees",
        description: "Success is not just making money. Success is happiness. Success is fulfillment; it's the ability to give.",

        pathImage: "assets/Money1.jpg",
        backgroundColor: Color(0xfff5a623),
      ),
    );
    slides.add(
      new Slide(
        title: "Penny For Your Thoughts",
        description: "Success is not just making money. Success is happiness. Success is fulfillment; it's the ability to give.",
        pathImage: "assets/Money2.jpg",
        backgroundColor: Color(0xff203152),
      ),
    );
    slides.add(
      new Slide(
        title: "Money is the Means, Not the End",
        description:
        "The key to making money is to stay invested",
        pathImage: "assets/Money3.jpg",
        backgroundColor: Color(0xff9932CC),
      ),
    );
  }

  void onDonePress() {
    // TODO: go to next screen
  }

  void onSkipPress() {
    // TODO: go to next screen
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: this.slides,
      onDonePress: (){

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Swipe_Page()),
        );
      },
      onSkipPress: this.onSkipPress,
    );
  }
}