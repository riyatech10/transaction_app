import 'package:flutter/material.dart';
class HomePageState extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePageState> with TickerProviderStateMixin {

  AnimationController _animationController;
 Animation  _animation;
  void mylistener(status){
    if(status == AnimationStatus.completed)
     {
       _animation.removeStatusListener(mylistener);
       _animationController.reset();
       _animation = Tween(begin: -0.1,end: 0.1).animate(CurvedAnimation
         (parent: _animationController,
           curve: Curves.ease)
       );
       _animationController.reverse();
     }
  }
 @override

 void initState(){
   super.initState();
   _animationController=AnimationController(
       duration: Duration(seconds: 8),
       vsync: this);
   _animation = Tween(begin: -0.1,end: 0.1).animate(CurvedAnimation
     (parent: _animationController,
       curve: Curves.ease)
   )..addStatusListener(mylistener);
   _animationController.forward();
 }

@override
  Widget build(BuildContext context) {
   final width =MediaQuery.of(context).size.width;
    // TODO: implement build
    return Scaffold(
      body: 
        AnimatedBuilder(animation: _animationController, builder: (context,child) =>
            Transform(
                transform: Matrix4.translationValues(0.0,_animationController.value*width,0.0),
          child: Center(
              child:  Card(
                child: new Column(
                  children: <Widget>[
                    new Container(
                      //color: Colors.red,
                      height: 200.0,

                      decoration: new BoxDecoration(
                        image: const DecorationImage(
                          fit: BoxFit.fill,
                          image: const AssetImage('assets/timeline.jpg'),
                        ),
                      ),
                      margin: EdgeInsets.all(10.0),
                    ),
                    new Container(
                      //color: Colors.red,
                      height: 200.0,
                      //width: 250.0,
                      decoration: new BoxDecoration(
                        image: const DecorationImage(
                          fit: BoxFit.fill,
                          image: const AssetImage('assets/timeline.jpg'),
                        ),
                      ),
                      margin: EdgeInsets.all(10.0),
                    ),

                  ],
                ),

              ),
          ),
            )
        )
    );
  }

}